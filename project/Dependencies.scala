import sbt._

object Dependencies {
  val circeVersion = "0.9.3"

  lazy val scalaTest = Seq("org.scalatest" %% "scalatest" % "3.0.5" % Test)
  lazy val akka = Seq(
    "com.typesafe.akka" %% "akka-http" % "10.1.3",
    "com.typesafe.akka" %% "akka-stream" % "2.5.12",
    "com.typesafe.akka" %% "akka-http-testkit" % "10.1.3" % Test
  )
  lazy val circe = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % circeVersion)

  lazy val akkaHttpCirce = Seq(
    "de.heikoseeberger" %% "akka-http-circe" % "1.21.0"
  )

  lazy val config = Seq(
    "com.typesafe" % "config" % "1.3.3"
  )

  lazy val dependencies = scalaTest ++ akka ++ circe ++ akkaHttpCirce ++ config
}

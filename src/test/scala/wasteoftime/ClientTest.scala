package wasteoftime

import akka.actor.ActorSystem
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.stream.ActorMaterializer
import de.heikoseeberger.akkahttpcirce._
import io.circe.generic.auto._
import org.scalatest.{AsyncFlatSpec, Matchers}
import wasteoftime.TestData._
import scala.concurrent.Future

class ClientTest extends AsyncFlatSpec with Matchers with FailFastCirceSupport {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  "withQuery" should "unmarshall when response is OK" in {
    val exampleResponse = Marshal(searchResults).to[HttpResponse]
    val client = new Client(_ => exampleResponse)
    client.withQuery("abc").map(_ shouldEqual Right(searchResults))
  }

  it should "return an empty list when nothing was found" in {
    val exampleResponse =
      Future.successful(HttpResponse(StatusCodes.BadRequest))
    val client = new Client(_ => exampleResponse)
    client.withQuery("abc").map(_ shouldEqual Right(SearchResults(List.empty)))
  }

  "totalTimeWastedInMinutes" should "return a sum of wasted time in minutes" in {
    val testSeries = Marshal(series).to[HttpResponse]
    val testSeason = Marshal(season).to[HttpResponse]
    val client = new Client(url => {
      val isSeason = url.uri.toString.contains("Season")
      if (isSeason) testSeason
      else testSeries
    })

    client
      .totalTimeWastedInMinutes("abc")
      .map(_ shouldEqual Right(WastedTime(120)))
  }
}

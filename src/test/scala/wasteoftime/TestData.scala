package wasteoftime

object TestData {
  val searchResults = SearchResults(List(Result("Hello Ladies", "https://ia.media-imdb.com/poster.jpg", "tt2378794")))
  val series = Series("3", "20", "SuperNova3000")
  val season = Season(List(Episode(), Episode()))
}

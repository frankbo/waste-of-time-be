package wasteoftime

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import akka.http.scaladsl.testkit.ScalatestRouteTest
import Routes._
import akka.http.scaladsl.unmarshalling.Unmarshaller._
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import TestData._
import akka.http.scaladsl.server.Route

import scala.concurrent.Future

class RoutesTest
    extends FlatSpec
    with FailFastCirceSupport
    with Matchers
    with ScalatestRouteTest {

  private val mockedClient = new ClientTrait {
    override def withQuery(query: String): Future[Either[ErrorResult, SearchResults]] =
      Future.successful(Right(searchResults))
    override def totalTimeWastedInMinutes(imdbId: String): Future[Either[ErrorResult, WastedTime]] =
      Future.successful(Right(WastedTime(123)))
  }

  "search endpoint" should "handle get calls to the search endpoint" in {
    Get("/search?q=white") ~> route(mockedClient) ~> check {
      status shouldEqual StatusCodes.OK
    }
  }
  it should "reject when no search query is set" in {
    Get("/search") ~> Route.seal(route(mockedClient)) ~> check {
      responseAs[String] shouldEqual "Request is missing required query parameter 'q'"
    }
  }
  "imdb endpoint" should "handle get calls with an id" in {
    Get("/imdb?id=someId") ~> route(mockedClient) ~> check {
      status shouldEqual StatusCodes.OK
    }
  }
  it should "reject when no id is set" in {
    Get("/imdb") ~> Route.seal(route(mockedClient)) ~> check {
      status shouldEqual StatusCodes.NotFound
      responseAs[String] shouldEqual "Request is missing required query parameter 'id'"
    }
  }
}

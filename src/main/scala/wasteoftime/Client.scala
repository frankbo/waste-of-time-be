package wasteoftime

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import io.circe.generic.auto._
import de.heikoseeberger.akkahttpcirce._
import com.typesafe.config.ConfigFactory
import scala.concurrent.{ExecutionContext, Future}
import Utils._
import akka.http.scaladsl.Http
import io.circe.Decoder

import scala.annotation.tailrec

case class Result(Title: String, Poster: String, imdbID: String)

case class SearchResults(Search: List[Result])

case class Series(totalSeasons: String, Runtime: String, Title: String)

case class Episode()

case class Season(Episodes: List[Episode])

case class WastedTime(timeInMinutes: Int)

case class ErrorResult(Response: String, Error: String)

trait ClientTrait {
  def withQuery(query: String): Future[Either[ErrorResult, SearchResults]]

  def totalTimeWastedInMinutes(
      imdbId: String): Future[Either[ErrorResult, WastedTime]]
}

object Client {
  def apply()(implicit m: ActorMaterializer,
              ex: ExecutionContext,
              as: ActorSystem): Client = {
    new Client(Http().singleRequest(_))
  }

  implicit def searchResultErrorDecoder(
      implicit resultDecoder: Decoder[SearchResults],
      errorDecoder: Decoder[ErrorResult])
    : Decoder[Either[ErrorResult, SearchResults]] =
    Decoder.decodeHCursor.flatMap(cursor => {
      if (cursor.downField("Error").succeeded) errorDecoder.map(Left(_))
      else resultDecoder.map(Right(_))
    })

  implicit def seasonErrorDecoder(implicit resultDecoder: Decoder[Season],
                                  errorDecoder: Decoder[ErrorResult])
    : Decoder[Either[ErrorResult, Season]] =
    Decoder.decodeHCursor.flatMap(cursor => {
      if (cursor.downField("Error").succeeded) errorDecoder.map(Left(_))
      else resultDecoder.map(Right(_))
    })
}

class Client(executeRequest: HttpRequest => Future[HttpResponse])(
    implicit m: ActorMaterializer,
    ex: ExecutionContext,
    as: ActorSystem)
    extends ClientTrait
    with FailFastCirceSupport {
  // Prefer own implicits over circes
  import Client._
  private val appConfig = ConfigFactory.load("application.conf")
  private val baseUrl = appConfig.getString("config.baseUrl")
  private val apiKey = appConfig.getString("config.apiKey")
  val seriesType = "&type=series"

  private def seasonQueryParam(seasonNr: Int): String = s"&Season=$seasonNr"

  private def withImdbId(imdbId: String): Future[Series] =
    executeRequest(
      HttpRequest(
        uri = baseUrl + s"?apikey=$apiKey" + seriesType + s"&i=$imdbId"))
      .flatMap(r => {
        if (r.status == StatusCodes.OK) Unmarshal(r.entity).to[Series]
        else Future.failed(new Exception("Couldn't unmarshal Series"))
      })

  private def withSeason(imdbId: String,
                         season: Int): Future[Either[ErrorResult, Season]] =
    executeRequest(
      HttpRequest(
        uri = baseUrl + s"?apikey=$apiKey" + seriesType + s"&i=$imdbId" + seasonQueryParam(
          season))).flatMap(r => {
      if (r.status == StatusCodes.OK)
        Unmarshal(r).to[Either[ErrorResult, Season]]
      else Future.failed(new Exception("Couldn't unmarshal Season"))
    })

  @tailrec
  private def allSeasons(imdbId: String,
                         season: Int,
                         acc: List[Future[Either[ErrorResult, Season]]])
    : List[Future[Either[ErrorResult, Season]]] =
    season match {
      case 0 => acc
      case _ =>
        allSeasons(imdbId, season - 1, withSeason(imdbId, season) :: acc)
    }

  def withQuery(query: String): Future[Either[ErrorResult, SearchResults]] =
    executeRequest(
      HttpRequest(
        uri = baseUrl + s"?apikey=$apiKey" + seriesType + s"&s=$query"))
      .flatMap(r => {
        if (r.status == StatusCodes.OK)
          Unmarshal(r.entity).to[Either[ErrorResult, SearchResults]]
        else Future.successful(Right(SearchResults(List.empty)))
      })

  def totalTimeWastedInMinutes(
      imdbId: String): Future[Either[ErrorResult, WastedTime]] =
    withImdbId(imdbId).flatMap(series => {
      val runtimeInMinutes =
        toInt(series.Runtime.filter(_.isDigit)).getOrElse(0)
      val totalSeasons = toInt(series.totalSeasons).getOrElse(0)

      Future
        .traverse(allSeasons(imdbId, totalSeasons, List.empty))(_.map({
          case Right(season) => Right(season.Episodes.length)
          case Left(a)       => Left(a)
        }))
        .map(l =>
          sequenceListOfEither(l).map(allLength =>
            WastedTime(allLength.sum * runtimeInMinutes)))
    })

  def sequenceListOfEither[A, B](list: List[Either[A, B]]): Either[A, List[B]] =
    list match {
      case x :: xs =>
        x.flatMap(xx => sequenceListOfEither(xs).map(yy => xx :: yy))
      case Nil => Right(Nil)
    }

}

package wasteoftime

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import io.circe.generic.auto._
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport

import scala.concurrent.ExecutionContext

object Routes extends FailFastCirceSupport {
  def route(client: ClientTrait)(implicit m: ActorMaterializer,
            ex: ExecutionContext,
            as: ActorSystem): Route = {
    path("search") {
      parameter("q".as[String]) { q =>
        get {
          complete(client.withQuery(q))
        }
      }
    } ~ path("imdb") {
      parameter("id".as[String]) { imdbId =>
        get {
          complete(client.totalTimeWastedInMinutes(imdbId))
        }
      }
    }
  }
}

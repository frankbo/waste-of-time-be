package wasteoftime

object Utils {
  def toInt(s: String): Option[Int] =
    try {
      Some(s.toInt)
    } catch {
      case _: Exception => None
    }
}
